# from redis import Redis
# import logging
#
# logging.basicConfig(level=logging.INFO)
# redis = Redis(host='redistest.wqduw1.0001.usw2.cache.amazonaws.com:6379', port=6379, decode_responses=True, ssl=True)
#
# if redis.ping():
#     logging.info("Connected to Redis")
import os
import redis
from flask import Flask, session, redirect, escape, request

app = Flask(__name__)

app.secret_key = "123456"

# BEGIN NEW CODE - PART 1 #
REDIS_URL = 'redis://redistest.wqduw1.0001.usw2.cache.amazonaws.com:6379'
store = redis.Redis.from_url(REDIS_URL)
print(store)
client = redis.Redis.from_url('redis://reditestpurpose.wqduw1.ng.0001.usw2.cache.amazonaws.com:6379')
print(client.ping())
# END NEW CODE - PART 1 #

@app.route('/')
def index():
    print("hi")
    if 'username' in session:
        print("sessio")

        # BEGIN NEW CODE - PART 2 #
        username = escape(session['username'])
        visits = store.hincrby(username, 'visits', 1)

    return '''
        Logged in as {0}.<br>
        Visits: {1}
        '''.format(username, visits)
        # END NEW CODE - PART 2 #

    return 'You are not logged in'


@app.route('/login', methods=['GET', 'POST'])
def login():
    print(request.get_json()["username"])

    if request.method == 'POST':
        session['username'] = request.get_json()["username"]
        # return redirect('/')
        return "success"

    # return '''
    #     <form method="post">
    #     <p><input type=text name=username>
    #     <p><input type=submit value=Login>
    #     </form>'''

@app.route('/logout')

def logout():

    session.pop('username', None)
    return redirect('/')

if __name__ == '__main__':

    app.run(host='127.0.0.1', port=8080, debug=True, use_reloader=True)