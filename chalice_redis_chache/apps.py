from chalice import Chalice
import boto3
from boto3.dynamodb.conditions import Key
import os
import psycopg2


def get_db_connection():
    conn = psycopg2.connect(
        host="database-1.cjvfkkbk0ydq.us-east-1.rds.amazonaws.com",
        database="postgred_db",
        user="postgred_user",
        password="postgredsqls")
    return conn

# Open a cursor to perform database operations
conn = get_db_connection()
cur=conn.cursor()
cur.execute('DROP TABLE IF EXISTS books;')
cur.execute('CREATE TABLE books (id serial PRIMARY KEY,'
                                 'title varchar (150) NOT NULL,'
                                 'author varchar (50) NOT NULL,'
                                 'pages_num integer NOT NULL,'
                                 'review text,'
                                 'date_added date DEFAULT CURRENT_TIMESTAMP);'
                                 )
app = Chalice(app_name='chalice_redis_chache')
app.debug=True


@app.route('/insert/db', methods=['POST'])
def index():
    conn=get_db_connection()
    cur = conn.cursor()
    cur.execute('INSERT INTO books (title, author, pages_num, review)'
                'VALUES (%s, %s, %s, %s)',
                ('A Tale of Two Cities',
                 'Charles Dickens',
                 489,
                 'A great classic!')
                )

    conn.commit()

    cur.close()
    conn.close()

    return {'message': "data inserted suucessfully"}


@app.route('/get/books/details',methods=['GET'])
def index():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('SELECT * FROM books;')
    books = cur.fetchall()
    cur.close()
    conn.close()
    return {'data':books}


# @app.route('/item/{id}', methods=['GET'])
# def item_get(id):
#     response = table.query(
#         KeyConditionExpression=Key("id").eq(id)
#     )
#     data = response.get('Items', None)
#
#     return {'data': data}
# from chalice import Chalice
# import boto3
# from boto3.dynamodb.conditions import Key
#
#
# app = Chalice(app_name='chalice_redis_chache')
# app.debug=True
# dynamodb = boto3.resource("dynamodb")
# table = dynamodb.Table('blogdb')
# # print(boto3.resource(dynamodb, "us-west-2").get_available_subresources()
# # )
# # print("tables",boto3.client(dynamodb, "us-west-2").list_tables()
# # )
#
#
# @app.route('/', methods=['GET'])
# def index():
#     response = table.scan()
#     data = response.get('Items', None)
#
#     return {'data': data}
#
#
# @app.route('/item/{id}', methods=['GET'])
# def item_get(id):
#     response = table.query(
#         KeyConditionExpression=Key("id").eq(id)
#     )
#     data = response.get('Items', None)
#
#     return {'data': data}
#
#
# @app.route('/item', methods=['POST'])
# def item_set():
#     data = app.current_request.json_body
#
#     try:
#         table.put_item(Item={
#             "id": data['id'],
#             "text": data['text']
#         })
#
#         return {'message': 'ok', 'status': 201, "id": data['id']}
#     except Exception as e:
#         return {'message': str(e)}
