from chalice import Chalice
import os
import psycopg2
import redis

REDIS_HOST='redistest.wqduw1.0001.usw2.cache.amazonaws.com'
REDIS_PORT=6379
red = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=0)
# print(red.ping())

def get_db_connection():
    # import pdb
    # pdb.set_trace();
    conn = psycopg2.connect(
        host='database-1.cjvfkkbk0ydq.us-east-1.rds.amazonaws.com',
        database="postgred_db",
        user="postgred_user",
        password="postgredsqls",
    )
    # cur = conn.cursor()
    # cur.execute('DROP TABLE IF EXISTS books;')
    # cur.execute('CREATE TABLE books (id serial PRIMARY KEY,'
    #             'title varchar (150) NOT NULL,'
    #             'author varchar (50) NOT NULL,'
    #             'pages_num integer NOT NULL,'
    #             'review text,'
    #             'date_added date DEFAULT CURRENT_TIMESTAMP);'
    #             )
    return conn

# Open a cursor to perform database operations
conn = get_db_connection()
app = Chalice(app_name='chalice_redis_chache')
app.debug=True


# @app.route('/insert/db', methods=['POST'])
# def indexs():
#     print("hrllo")
#     import pdb
#     pdb.set_trace();
#     conn=get_db_connection()
#     cur = conn.cursor()
#     # cur.execute('DROP TABLE IF EXISTS books;')
#     # cur.execute('CREATE TABLE books (id serial PRIMARY KEY,'
#     #             'title varchar (150) NOT NULL,'
#     #             'author varchar (50) NOT NULL,'
#     #             'pages_num integer NOT NULL,'
#     #             'review text,'
#     #             'date_added date DEFAULT CURRENT_TIMESTAMP);'
#     #             )
#     cur.execute('INSERT INTO books (title, author, pages_num, review)'
#                 'VALUES (%s, %s, %s, %s)',
#                 ('A Tale of Two Cities',
#                  'Charles Dickens',
#                  489,
#                  'A great classic!')
#                 )
#       movies = cur.fetchall()
#       for data in movies:
#           red.hset( "movies", data)
#     print("cur",cur)
#
#     conn.commit()
#
#     cur.close()
#     conn.close()
#     print("last")
#
#     return {'message': "data inserted suucessfully"}

@app.route('/get/movies/details',methods=['GET'])
def index():
    # import pdb
    # pdb.set_trace()
    movies_data= red.hgetall("movies")
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('SELECT * FROM movies;')
    movies = cur.fetchall()
    cur.close()
    conn.close()
    return {'data':movies}
